#include "checker.h"

void Checker::startTimer(void) {
  m_startTimer = std::chrono::steady_clock::now();
}

void Checker::finishTimer(void) {
  m_endTimer = std::chrono::steady_clock::now();
}

int Checker::duration(void) const {
  return std::chrono::duration_cast<std::chrono::milliseconds>(m_endTimer -
                                                               m_startTimer)
      .count();
}

void Checker::addCheckedWordCount(const size_t checkedWordCount) {
  m_checkedWordCount += checkedWordCount;
}

size_t Checker::getCheckedWordCount(void) const { return m_checkedWordCount; }

void Checker::addIncorrectWordCount(const size_t incorrectWordCount) {
  m_incorrectWordCount += incorrectWordCount;
}

size_t Checker::getIncorrectWordCount(void) const {
  return m_incorrectWordCount;
}

void Checker::setDataStructureName(const std::string& dataStructureName) {
  m_dataStructureName = dataStructureName;
}

std::string Checker::getDataStructureName(void) const {
  return m_dataStructureName;
}