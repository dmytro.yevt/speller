#ifndef SPELLER_STRINGCORRECTION_H
#define SPELLER_STRINGCORRECTION_H

#include <string>

namespace stringCorrection {
void convertToLowerCase(std::string &);
bool isInvalidCharacter(char);
void removeUnnecessary(std::string &);
}  // namespace stringCorrection

#endif  // SPELLER_STRINGCORRECTION_H
