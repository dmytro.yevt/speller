# Speller

#### About project
The project is designed to visualize the working time of data structures of two types: standard (std::vector and std::set) and written by us (hash table and binary search tree).

#### How it works
You must upload the dictionary.txt into the project folder and create a "text" folder there that contains the texts.

#### My computer results

###### Computer specifications:

CPU:
> Intel Core i5-7200U 2.5GHz

RAM & Speed:
> 8GB DDR4

###### Results:

| Data structure | Dictionary load time | Texts processing time | The total number of words | Number of incorrect words |
|:--------------:|:--------------------:|:---------------------:|:-------------------------:|:-------------------------:|
| hashtable      | 328                  | 6157730               | 2002324                   | 55014                     |
| bintree        | 802                  | 4448                  | 2002324                   | 55014                     |
| stdvector      | 193                  | 3582                  | 2002324                   | 55014                     |
| stdordset      | 356                  | 3788                  | 2002324                   | 55014                     |


Attention! Your results may be different from mine!