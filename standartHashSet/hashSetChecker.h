#ifndef SPELLER_HASHSETCHECKER_H
#define SPELLER_HASHSETCHECKER_H

#include <set>
#include <string>

#include "../checker.h"

class HashSetChecker : public Checker {
 public:
  HashSetChecker(void);
  void add(const std::string &) override final;
  bool check(const std::string &) const override final;

 private:
  std::set<std::string> m_hashSet;
  // std::map<size_t, std::string> m_hashMap;
};

#endif  // SPELLER_HASHSETCHECKER_H
