#include "hashSetChecker.h"

HashSetChecker::HashSetChecker(void) { setDataStructureName("stdorset"); }

void HashSetChecker::add(const std::string& word) { m_hashSet.emplace(word); }

bool HashSetChecker::check(const std::string& word) const {
  return (m_hashSet.find(word) != m_hashSet.end());
}