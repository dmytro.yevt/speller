#ifndef SPELLER_CHECKER_H
#define SPELLER_CHECKER_H

#include <chrono>
#include <string>

class Checker {
 public:
  Checker(void) : m_checkedWordCount{0u}, m_incorrectWordCount{0u} {}
  virtual ~Checker() {}
  virtual void add(const std::string &) = 0;
  virtual bool check(const std::string &) const = 0;
  void startTimer(void);
  void finishTimer(void);
  int duration(void) const;
  void addCheckedWordCount(const size_t);
  size_t getCheckedWordCount(void) const;
  void addIncorrectWordCount(const size_t);
  size_t getIncorrectWordCount(void) const;
  std::string getDataStructureName(void) const;

 protected:
  void setDataStructureName(const std::string &);

 protected:
  std::chrono::time_point<std::chrono::steady_clock> m_startTimer, m_endTimer;
  size_t m_checkedWordCount, m_incorrectWordCount;
  std::string m_dataStructureName;
};

#endif  // SPELLER_CHECKER_H
