#include "iofile.h"

#include <exception>
#include <fstream>

#include "stringCorrection.h"

void ioFile::loadDictionary(Checker* dataStructure) {
  const std::string dictionaryPath{"../dictionary.txt"};
  std::ifstream dictionary(dictionaryPath, std::ios_base::in);
  if (!dictionary.is_open()) {
    const std::string error{"The file " + dictionaryPath + " cannot be open"};
    throw std::invalid_argument(error);
  }
  std::string word;
  dataStructure->startTimer();
  while (!dictionary.eof()) {
    dictionary >> word;
    dataStructure->add(word);
  }
  dataStructure->finishTimer();
}

std::vector<std::string> ioFile::getFilesList(void) {
  std::vector<std::string> filesList;
  const fs::path directory{"../texts"};
  if (!isDirExists(directory)) {
    const std::string error{"The directory " + directory.string() +
                            " is empty. Put files into "};
    throw std::invalid_argument(error);
  }
  for (const auto& file : fs::directory_iterator(directory)) {
    const std::string fileName{file.path().filename().string()};
    filesList.emplace_back(fileName);
  }
  return filesList;
}

void ioFile::textProcessing(const std::string& fileName,
                            Checker* dataStructure) {
  std::ifstream textFile("../texts/" + fileName, std::ios_base::in);
  if (!textFile.is_open()) {
    const std::string error{"The file " + fileName + " cannot be open"};
    throw std::invalid_argument(error);
  }

  const std::string incorrectWordsPath{"../incorrectWords"};
  isDirExists(incorrectWordsPath);
  const std::string incorrectWordsFilePath{incorrectWordsPath + "/incorrect_" +
                                           fileName};
  std::ofstream incorrectWords(incorrectWordsFilePath, std::ifstream::trunc);
  if (!incorrectWords.is_open()) {
    const std::string error{"The file " + incorrectWordsFilePath +
                            " cannot be open"};
    throw std::invalid_argument(error);
  }

  size_t incorrectWordCount{0};
  size_t checkedWordCount{0};
  std::string word;
  dataStructure->startTimer();
  while (!textFile.eof()) {
    textFile >> word;
    stringCorrection::removeUnnecessary(word);
    if (!word.empty()) {
      stringCorrection::convertToLowerCase(word);
      if (!dataStructure->check(word)) {
        incorrectWords << word << '\n';
        ++incorrectWordCount;
      }
    }
    ++checkedWordCount;
  }
  dataStructure->finishTimer();

  dataStructure->addCheckedWordCount(checkedWordCount);
  dataStructure->addIncorrectWordCount(incorrectWordCount);
}

bool ioFile::isDirExists(const fs::path& dirPath) {
  bool isExist{false};

  if (!fs::exists(dirPath)) {
    fs::create_directory(dirPath);
  } else {
    isExist = true;
  }

  return isExist;
}