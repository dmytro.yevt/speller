#include "stringCorrection.h"

#include <algorithm>
#include <cctype>

void stringCorrection::convertToLowerCase(std::string& word) {
  std::transform(word.begin(), word.end(), word.begin(),
                 static_cast<int (*)(int)>(std::tolower));
}

bool stringCorrection::isInvalidCharacter(char ch) {
  return !((isalpha(ch) != 0) || (ch == '\''));
}

void stringCorrection::removeUnnecessary(std::string& word) {
  const auto removePositions =
      std::remove_if(word.begin(), word.end(), isInvalidCharacter);
  word.erase(removePositions, word.end());
  while (word[0] == '\'') {
    word.erase(word.begin());
  }
}