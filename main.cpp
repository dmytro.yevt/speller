// #include "hashtable/hashChecker.h"
// #include "binarysearchtree/binSearchTreeChecker.h"
// #include "standartVector/vectorChecker.h"
// #include "standartHashSet/hashSetChecker.h"
#include <exception>
#include <iostream>

#include "iofile.h"
#include "trie/trieChecker.h"

void showInformation(const Checker *, int, int);

int main(void) {
  const auto files{ioFile::getFilesList()};

  //    HashChecker checkHashTable;
  //    BinSearchTreeChecker checkBinTree;
  //    StdVectorChecker checkVector;
  //    HashSetChecker checkMap;

  TrieChecker checkTrie;

  try {
    ioFile::loadDictionary(&checkTrie);
    const int trieLoadDictionaryTime{checkTrie.duration()};
    int trieProccessingFileTime{0};
    for (const auto &file : files) {
      ioFile::textProcessing(file, &checkTrie);
      trieProccessingFileTime += checkTrie.duration();
    }

    showInformation(&checkTrie, trieLoadDictionaryTime,
                    trieProccessingFileTime);

    /*ioFile::loadDictionary(&checkHashTable);
    const int hashTableLoadDictionaryTime { checkHashTable.duration() };
    int hashTableProccessingFileTime { 0 };
    for(const auto & file : files)
    {
        ioFile::textProcessing(file, &checkHashTable);
        hashTableProccessingFileTime += checkHashTable.duration();
    }

    ioFile::loadDictionary(&checkVector);
    const int vectorLoadDictionaryTime { checkVector.duration() };
    int vectorProccessingFileTime { 0 };
    for(const auto & file : files)
    {
        ioFile::textProcessing(file, &checkVector);
        vectorProccessingFileTime += checkVector.duration();
    }

    ioFile::loadDictionary(&checkMap);
    const int mapLoadDictionaryTime { checkMap.duration() };
    int mapProccessingFileTime { 0 };
    for(const auto & file : files)
    {
        ioFile::textProcessing(file, &checkMap);
        mapProccessingFileTime += checkMap.duration();
    }

    ioFile::loadDictionary(&checkBinTree);
    const int binTreeLoadDictionaryTime { checkBinTree.duration() };
    int binTreeProccessingFileTime { 0 };
    for(const auto & file : files)
    {
        ioFile::textProcessing(file, &checkBinTree);
        binTreeProccessingFileTime += checkBinTree.duration();
    }

    showInformation(&checkHashTable, hashTableLoadDictionaryTime,
    hashTableProccessingFileTime); showInformation(&checkBinTree,
    binTreeLoadDictionaryTime, binTreeProccessingFileTime);
    showInformation(&checkVector, vectorLoadDictionaryTime,
    vectorProccessingFileTime); showInformation(&checkMap,
    mapLoadDictionaryTime, mapProccessingFileTime);*/
  } catch (const std::exception &error) {
    std::cerr << error.what() << std::endl;
  }
  return 0;
}

void showInformation(const Checker *dataStructure, const int loadDictionaryTime,
                     const int processingFileTime) {
  std::cout << dataStructure->getDataStructureName() << ' '
            << loadDictionaryTime << ' ' << processingFileTime << ' '
            << dataStructure->getCheckedWordCount() << ' '
            << dataStructure->getIncorrectWordCount() << std::endl;
}