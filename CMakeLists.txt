cmake_minimum_required(VERSION 3.1)
project(Speller)

set(CMAKE_CXX_STANDARD 17)

add_executable(Speller main.cpp checker.cpp
        hashtable/hashChecker.cpp hashtable/hashtable.cpp
        standartVector/vectorChecker.cpp standartHashSet/hashSetChecker.cpp
        binarysearchtree/binSearchTreeChecker.cpp binarysearchtree/binarysearchtree.cpp
        trie/trie.cpp trie/trieChecker.cpp
        stringCorrection.cpp iofile.cpp)

target_link_libraries(Speller stdc++fs)

add_subdirectory(googletest)
include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})
add_executable(Speller_utest unit_tests.cpp checker.cpp stringCorrection.cpp
        hashtable/hashChecker.cpp hashtable/hashtable.cpp
        standartVector/vectorChecker.cpp standartHashSet/hashSetChecker.cpp
        binarysearchtree/binSearchTreeChecker.cpp binarysearchtree/binarysearchtree.cpp
        trie/trie.cpp trie/trieChecker.cpp)
target_link_libraries(Speller_utest gtest gtest_main)

target_compile_options(Speller PRIVATE -Wall -Wextra -pedantic -Werror)