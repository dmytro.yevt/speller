#ifndef SPELLER_VECTORCHECKER_H
#define SPELLER_VECTORCHECKER_H

#include <vector>

#include "../checker.h"

class StdVectorChecker : public Checker {
 public:
  StdVectorChecker(void);
  void add(const std::string &) override final;
  bool check(const std::string &) const override final;

 private:
  std::vector<std::string> m_vector;
};

#endif  // SPELLER_VECTORCHECKER_H
