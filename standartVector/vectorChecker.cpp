#include "vectorChecker.h"

#include <algorithm>

StdVectorChecker::StdVectorChecker(void) { setDataStructureName("stdvector"); }

void StdVectorChecker::add(const std::string& word) {
  m_vector.emplace_back(word);
}

bool StdVectorChecker::check(const std::string& word) const {
  return std::binary_search(m_vector.cbegin(), m_vector.cend(), word);
}