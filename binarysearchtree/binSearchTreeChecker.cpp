#include "binSearchTreeChecker.h"

BinSearchTreeChecker::BinSearchTreeChecker(void) {
  setDataStructureName("bintree");
}

void BinSearchTreeChecker::add(const std::string& word) {
  m_binaryTree.insert(word);
}

bool BinSearchTreeChecker::check(const std::string& word) const {
  return m_binaryTree.find(word);
}