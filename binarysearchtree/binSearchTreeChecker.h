#ifndef SPELLER_BINSEARCHTREECHECKER_H
#define SPELLER_BINSEARCHTREECHECKER_H

#include "../checker.h"
#include "binarysearchtree.h"

class BinSearchTreeChecker : public Checker {
 public:
  BinSearchTreeChecker(void);
  void add(const std::string &) override final;
  bool check(const std::string &) const override final;

 private:
  BST m_binaryTree;
};

#endif  // SPELLER_BINSEARCHTREECHECKER_H
