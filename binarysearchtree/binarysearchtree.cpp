#include "binarysearchtree.h"

#include <iostream>

BST::BST(void) : root(nullptr) {}

BST::~BST(void) {}

void BST::insert(const std::string& value) { insertNode(root, value); }

BST::TreeNodePtr& BST::insertNode(TreeNodePtr& node, const std::string& value) {
  if (node == nullptr) {
    node = std::make_shared<TreeNode>(value);
  } else if (value < node->data) {
    node->leftPtr = insertNode(node->leftPtr, value);
    if (height(node->leftPtr) - height(node->rightPtr) == allowableHeight) {
      if (value < node->leftPtr->data) {
        node = rotateWithLeftChild(node);
      } else {
        node = doubleWithLeftChild(node);
      }
    }
  } else if (value > node->data) {
    node->rightPtr = std::move(insertNode(node->rightPtr, value));
    if (height(node->rightPtr) - height(node->leftPtr) == allowableHeight) {
      if (value > node->rightPtr->data) {
        node = rotateWithRightChild(node);
      } else {
        node = doubleWithRightChild(node);
      }
    }
  }
  node->height = max(height(node->leftPtr), height(node->rightPtr)) + 1;
  return node;
}

size_t BST::height(TreeNodePtr& node) const {
  return (node == nullptr) ? 0u : node->height;
}

size_t BST::max(const size_t leftNum, const size_t rightNum) const {
  return (leftNum > rightNum) ? leftNum : rightNum;
}

BST::TreeNodePtr BST::rotateWithLeftChild(TreeNodePtr k2) {
  TreeNodePtr k1 = k2->leftPtr;
  k2->leftPtr = k1->rightPtr;
  k1->rightPtr = k2;

  k2->height = max(height(k2->leftPtr), height(k2->rightPtr)) + 1;
  k1->height = max(height(k1->leftPtr), height(k2)) + 1;
  return k1;
}

BST::TreeNodePtr BST::rotateWithRightChild(TreeNodePtr k1) {
  TreeNodePtr k2 = k1->rightPtr;
  k1->rightPtr = k2->leftPtr;
  k2->leftPtr = k1;

  k1->height = max(height(k1->leftPtr), height(k1->rightPtr)) + 1;
  k2->height = max(height(k2->rightPtr), height(k1)) + 1;
  return k2;
}

BST::TreeNodePtr BST::doubleWithLeftChild(TreeNodePtr k3) {
  k3->leftPtr = rotateWithRightChild(k3->leftPtr);
  return rotateWithLeftChild(k3);
}

BST::TreeNodePtr BST::doubleWithRightChild(TreeNodePtr k1) {
  k1->rightPtr = rotateWithLeftChild(k1->rightPtr);
  return rotateWithRightChild(k1);
}

void BST::deleteNode(const std::string& data) { deleteNode(root, data); }

void BST::deleteNode(TreeNodePtr& node, const std::string& data) {
  TreeNodePtr tempNodePtr;
  if (node == nullptr) return;

  if (data > node->data) {
    deleteNode(node->rightPtr, data);
  } else if (data < node->data) {
    deleteNode(node->leftPtr, data);
  } else {
    if (node->leftPtr == nullptr && node->rightPtr == nullptr) {
      node = nullptr;
    } else if (node->leftPtr != nullptr && node->rightPtr == nullptr) {
      node = std::move(node->leftPtr);
    } else if (node->leftPtr == nullptr && node->rightPtr != nullptr) {
      node = std::move(node->rightPtr);
    } else {
      try {
        TreeNodePtr& minNode = findMinimal(node->rightPtr);
        node->data = minNode->data;
        deleteNode(minNode, minNode->data);
      } catch (int) {
        TreeNodePtr& minNode = node->rightPtr;
        node->data = minNode->data;
        deleteNode(minNode, minNode->data);
      }
    }
  }
}

bool BST::find(const std::string& data) const {
  try {
    const TreeNodePtr& resultOfFind = findNode(root, data);
    return (resultOfFind != nullptr);
  } catch (int) {
    return false;
  }
}

const BST::TreeNodePtr& BST::findNode(const TreeNodePtr& node,
                                      const std::string& data) const {
  if (node == nullptr) throw 0;

  if (data < node->data)
    return findNode(node->leftPtr, data);
  else if (data > node->data)
    return findNode(node->rightPtr, data);
  else
    return node;
}

BST::TreeNodePtr& BST::findMinimal(TreeNodePtr& node) const {
  if (node == nullptr) throw 0;

  if (node->leftPtr == nullptr)
    return node;
  else
    return findMinimal(node);
}

void BST::printTree(void) const {
  inOrder(root);
  std::cout << std::endl;
}

void BST::inOrder(const TreeNodePtr& node) const {
  if (node != nullptr) {
    std::cout << node->data << ' ';
    inOrder(node->leftPtr);
    inOrder(node->rightPtr);
  }
}