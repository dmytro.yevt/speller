#ifndef BINARYSEARCHTREE_BINARYSEARCHTREE_H
#define BINARYSEARCHTREE_BINARYSEARCHTREE_H

#include <memory>
#include <string>
#include <utility>

class BST {
 private:
  struct TreeNode {
    explicit TreeNode(std::string data)
        : leftPtr(nullptr),
          rightPtr(nullptr),
          data(std::move(data)),
          height(0u) {}

    std::shared_ptr<TreeNode> leftPtr;
    std::shared_ptr<TreeNode> rightPtr;
    std::string data;
    size_t height;
  };
  using TreeNodePtr = std::shared_ptr<TreeNode>;

 public:
  BST(void);
  ~BST(void);
  void insert(const std::string &);
  void printTree(void) const;
  void deleteNode(const std::string &);
  bool find(const std::string &) const;

 private:
  TreeNodePtr &insertNode(TreeNodePtr &, const std::string &);
  void deleteNode(TreeNodePtr &, const std::string &);
  void inOrder(const TreeNodePtr &) const;
  const TreeNodePtr &findNode(const TreeNodePtr &, const std::string &) const;
  TreeNodePtr &findMinimal(TreeNodePtr &) const;
  size_t height(TreeNodePtr &) const;
  size_t max(size_t, size_t) const;

  TreeNodePtr rotateWithLeftChild(TreeNodePtr);
  TreeNodePtr rotateWithRightChild(TreeNodePtr);
  TreeNodePtr doubleWithLeftChild(TreeNodePtr);
  TreeNodePtr doubleWithRightChild(TreeNodePtr);

 private:
  TreeNodePtr root;
  const size_t allowableHeight{2u};
};

#endif  // BINARYSEARCHTREE_BINARYSEARCHTREE_H
