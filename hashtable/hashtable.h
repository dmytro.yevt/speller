#ifndef TEMP_HASHTABLE_H
#define TEMP_HASHTABLE_H

#include <string>
#include <vector>

class hashTable {
 public:
  hashTable(const size_t);
  void show(void) const;
  void add(const std::string &);
  bool check(const std::string &) const;
  size_t getMaxCollisions(void) const;

 private:
  bool isEmpty(void) const;
  void resize(void);
  size_t hashEncoding(const std::string &) const;

 private:
  size_t m_size;
  std::vector<std::vector<std::string>> m_hash;
};

#endif  // TEMP_HASHTABLE_H
