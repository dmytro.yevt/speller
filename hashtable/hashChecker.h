#ifndef SPELLER_HASHCHECKER_H
#define SPELLER_HASHCHECKER_H

#include "../checker.h"
#include "hashtable.h"

class HashChecker : public Checker {
 public:
  HashChecker(void);
  void add(const std::string &) override final;
  bool check(const std::string &) const override final;

 private:
  hashTable m_hash;
};

#endif  // SPELLER_HASHCHECKER_H
