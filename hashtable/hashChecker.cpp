#include "hashChecker.h"

HashChecker::HashChecker(void) : m_hash(27u) {
  setDataStructureName("hashtable");
}

void HashChecker::add(const std::string& string) { m_hash.add(string); }

bool HashChecker::check(const std::string& string) const {
  return m_hash.check(string);
}