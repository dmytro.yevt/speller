#include "hashtable.h"

#include <algorithm>
#include <iostream>

hashTable::hashTable(const size_t size)
    : m_size(size), m_hash(m_size, std::vector<std::string>()) {}

void hashTable::show(void) const {
  if (isEmpty()) {
    std::cout << "Hash table is empty" << std::endl;
    return;
  }

  for (const auto& elements : m_hash) {
    for (const auto& element : elements) {
      std::cout << element << ' ';
    }
    std::cout << std::endl;
  }
}

void hashTable::add(const std::string& string) {
  size_t hash{hashEncoding(string)};

  while (hash > m_size) {
    resize();
  }

  m_hash[hash].emplace_back(string);
}

bool hashTable::check(const std::string& string) const {
  bool resultFind{false};

  if (!isEmpty()) {
    const size_t hash{hashEncoding(string)};
    const std::vector<std::string> chain(m_hash[hash]);
    resultFind = std::binary_search(chain.cbegin(), chain.cend(), string);
  }

  return resultFind;
}

size_t hashTable::getMaxCollisions(void) const {
  size_t maxCollisions{0u};

  for (size_t i = 0; i < m_size; ++i) {
    const size_t chainSize{m_hash[i].size()};
    if (chainSize > maxCollisions) maxCollisions = chainSize;
  }

  return maxCollisions;
}

bool hashTable::isEmpty(void) const {
  bool isEmpty{true};

  auto firstNotEmpty{std::find_if(m_hash.cbegin(), m_hash.cend(),
                                  [](const std::vector<std::string>& elements) {
                                    return (elements.size() != 0);
                                  })};
  if (firstNotEmpty != m_hash.end()) isEmpty = false;

  return isEmpty;
}

void hashTable::resize(void) {
  m_size <<= 1;
  m_hash.resize(m_size, std::vector<std::string>());
}

size_t hashTable::hashEncoding(const std::string& string) const {
  return string[0] - 'a';
}