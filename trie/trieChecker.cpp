#include "trieChecker.h"

TrieChecker::TrieChecker(void)
{
    setDataStructureName("trie");
}

void TrieChecker::add(const std::string & word)
{
    m_trie.insert(word);
}

bool TrieChecker::check(const std::string & word) const
{
    return m_trie.search(word);
}