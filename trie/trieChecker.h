#ifndef SPELLER_TRIECHECKER_H
#define SPELLER_TRIECHECKER_H

#include "../checker.h"
#include "trie.h"

class TrieChecker : public Checker
{
public:
    TrieChecker(void);

    void add(const std::string &) override final;
    bool check(const std::string &) const override final;

private:
    Trie m_trie;
//   struct TrieNode * m_trie;
};

#endif //SPELLER_TRIECHECKER_H
