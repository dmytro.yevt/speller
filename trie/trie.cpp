#include "trie.h"

#include <iostream>
Trie::Trie(void) : m_root(new TrieNode) {}

Trie::~Trie(void) { m_root = remove(m_root); }

Trie::TrieNodePtr Trie::remove(Trie::TrieNodePtr node) {
  if (node == nullptr) {
    return nullptr;
  }

  for (auto& child : node->leaves) {
    if (child != nullptr) {
      child = remove(child);
    }
  }
  delete node;
  return node;
}

int Trie::hash(char ch) const {
  constexpr int MAXIMUM_INDEX{ALPHABET_SIZE - 1};
  return (ch != '\'') ? ch - 'a' : MAXIMUM_INDEX;
}

void Trie::insert(const std::string& word) {
  auto stepNode{m_root};

  for (const auto& ch : word) {
    const int index{hash(ch)};

    if (stepNode->leaves[index] == nullptr) {
      stepNode->leaves[index] = new TrieNode;
    }

    stepNode = stepNode->leaves[index];
  }

  stepNode->isEndOfWord = true;
}

bool Trie::search(const std::string& word) const {
  auto stepNode{m_root};

  for (const auto& ch : word) {
    const int index{hash(ch)};

    if (stepNode->leaves[index] == nullptr) {
      return false;
    }

    stepNode = stepNode->leaves[index];
  }

  return (stepNode != nullptr && stepNode->isEndOfWord);
}