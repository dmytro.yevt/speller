#ifndef SPELLER_TRIE_H
#define SPELLER_TRIE_H

#include <string>

class Trie {
 private:
  static constexpr size_t ALPHABET_SIZE{27u};
  struct TrieNode {
    TrieNode *leaves[ALPHABET_SIZE];
    bool isEndOfWord;

    TrieNode() : leaves{nullptr}, isEndOfWord(false) {}
  };
  using TrieNodePtr = TrieNode *;

  int hash(char) const;
  TrieNodePtr remove(TrieNodePtr);

 public:
  Trie(void);
  ~Trie(void);
  void insert(const std::string &);
  bool search(const std::string &) const;

 private:
  TrieNodePtr m_root;
};

#endif  // SPELLER_TRIE_H