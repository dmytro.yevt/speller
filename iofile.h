#ifndef SPELLER_IOFILE_H
#define SPELLER_IOFILE_H

#include <experimental/filesystem>
#include <vector>

#include "checker.h"

namespace ioFile {
namespace fs = std::experimental::filesystem;

void loadDictionary(Checker *dataStructure);
std::vector<std::string> getFilesList(void);
void textProcessing(const std::string &, Checker *);
bool isDirExists(const fs::path &);
}  // namespace ioFile

#endif  // SPELLER_IOFILE_H
