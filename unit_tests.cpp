#include "gtest/gtest.h"
#include "stringCorrection.h"

#include "hashtable/hashChecker.h"
#include "binarysearchtree/binSearchTreeChecker.h"
#include "standartVector/vectorChecker.h"
#include "standartHashSet/hashSetChecker.h"
#include "trie/trieChecker.h"

TEST(stringCorrection, correctingString)
{
    std::string word {"qwer.t4y"}, result {"qwerty"};
    stringCorrection::removeUnnecessary(word);
    EXPECT_EQ(result, word);
}

TEST(stringCorrection, convertToLowerCase)
{
    std::string word {"QWERTY"}, result {"qwerty"};
    stringCorrection::convertToLowerCase(word);
    EXPECT_EQ(result, word);
}

TEST(stringCorrection, complex)
{
    std::string word { "QW'ER.T4Y122x12" }, result {"qw'ertyx"};
    stringCorrection::removeUnnecessary(word);
    stringCorrection::convertToLowerCase(word);
    EXPECT_EQ(result, word);
}

TEST(dataStructure, hashSetCheck)
{
    const std::string dictionary[] {{"qwerty"}, {"test"}, {"help"}, {"global"}, {"logic"}};
    HashSetChecker hashSet;
    for(const auto & word : dictionary)
    {
        hashSet.add(word);
    }

    for(const auto & word : dictionary)
    {
        EXPECT_EQ(hashSet.check(word), true);
    }

    const std::string badWords[] {{"qwe"}, {"test1"}};
    for(const auto & word : badWords)
    {
        EXPECT_EQ(hashSet.check(word), false);
    }
}

TEST(dataStructure, stdVectorCheck)
{
    const std::string dictionary[] {{"atest"}, {"btest"}, {"chelp"}, {"dglobal"}, {"elogic"}};
    StdVectorChecker vectorCheck;
    for(const auto & word : dictionary)
    {
        vectorCheck.add(word);
    }

    for(const auto & word : dictionary)
    {
        EXPECT_EQ(vectorCheck.check(word), true);
    }

    const std::string badWords[] {{"qwe"}, {"test1"}};
    for(const auto & word : badWords)
    {
        EXPECT_EQ(vectorCheck.check(word), false);
    }
}

TEST(dataStructure, hashTableCheck)
{
    const std::string dictionary[] {{"qwerty"}, {"test"}, {"help"}, {"global"}, {"logic"}};
    HashChecker hashCheck;
    for(const auto & word : dictionary)
    {
        hashCheck.add(word);
    }

    for(const auto & word : dictionary)
    {
        EXPECT_EQ(hashCheck.check(word), true);
    }

    const std::string badWords[] {{"qwe"}, {"test1"}};
    for(const auto & word : badWords)
    {
        EXPECT_EQ(hashCheck.check(word), false);
    }
}

TEST(dataStructure, binarySearchTreeCheck)
{
    const std::string dictionary[] {{"qwerty"}, {"test"}, {"help"}, {"global"}, {"logic"}};
    BinSearchTreeChecker binTree;
    for(const auto & word : dictionary)
    {
        binTree.add(word);
    }

    for(const auto & word : dictionary)
    {
        EXPECT_EQ(binTree.check(word), true);
    }

    const std::string badWords[] {{"qwe"}, {"test1"}};
    for(const auto & word : badWords)
    {
        EXPECT_EQ(binTree.check(word), false);
    }
}

TEST(dataStructure, trieCheck)
{
    const std::string dictionary[] {{"qwerty"}, {"test"}, {"help"}, {"global"}, {"logic"}};
    TrieChecker trie;
    for(const auto & word : dictionary)
    {
        trie.add(word);
    }

    for(const auto & word : dictionary)
    {
        EXPECT_EQ(trie.check(word), true);
    }

    const std::string badWords[] {{"qwe"}, {"test1"}};
    for(const auto & word : badWords)
    {
        EXPECT_EQ(trie.check(word), false);
    }
}